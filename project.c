/**
  Alexander Glavin and Jacob Meyer
  601.220
  Midterm Project
  email: aglavin1@jhu.edu & jmeyer54@jhu.edu
  10/11/17
  jhedID: aglavin1 & jmeyer54
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "interface.h"
#include "functions.h"

/**
  Main function
*/
int main() {
  process_input();
}
