#  Alexander Glavin
#  601.220
#  HW 04
#  email: aglavin1@jhu.edu
#  09/24/17
#  jhedID: aglavin1
#  File adapted from sample file given in week 3 exercises 1 folder


CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra 

# Links together files needed to create executable
db: project.o functions.o interface.o
	$(CC) -lm -o project project.o functions.o interface.o

# Compiles mainFile.c to create mainFile.o
# Note that we list functions.h here as a file that
# mainFile.c depends on, since mainFile.c #includes it
main.o: project.c functions.h interface.h
	$(CC) $(CFLAGS) -c main.c

# Compiles functions.c to create functions.o
# Note that we list functions.h here as a file that
# functions.c depends on, since functions.c #includes it
functions.o: functions.c functions.h interface.h
	$(CC) $(CFLAGS) -c functions.c

interface.o: interface.c interface.h
	$(CC) $(CFLAGS) -c interface.c

# Removes all object files and the executable named main,
# so we can start fresh
clean:
	rm -f *.o db
