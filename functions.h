/**
  Alexander Glavin and Jacob Meyer
  601.220
  Midterm Project
  email: aglavin1@jhu.edu & jmeyer54@jhu.edu
  10/11/17
  jhedID: aglavin1 & jmeyer54
*/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdlib.h>
#include <stdio.h>

/**
  Processes input symbols and rejects bad ones
*/
void process_input();

#endif
