/**
  Primary file for holding all the functions for the assignment
  Alexander Glavin and Jacob Meyer
  601.220
  Midterm Project
  emails: aglavin1@jhu.edu & jmeyer54@jhu.edu
  10/11/17
  jhedID: aglavin1 & jmeyer54
*/

#include "interface.h"
#include "functions.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MAX_LINE_LENGTH 255
#define RGB_COLOR_SIZE 255
#define PI 3.14159265358979323846264

typedef struct{
  unsigned char red, green, blue;
} ppmPixel;

typedef struct{
  unsigned int width, height;
  ppmPixel *data;
} ppmImage;

float sq(float a) {
  return a * a;
}

unsigned char saturate(int input) {
  if (input > 255) {
    return 255;
  } else if (input < 0) {
    return 0;
  }
  return input;
}

void grayscale_ppm(ppmImage *img) {
  print_grayscale();
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      ppmPixel *pixel = img->data + x + y * (img->width);
      double gray = 0.3 * pixel->red + 0.59 * pixel->green + 0.11 * pixel->blue;
      pixel->red = (unsigned char) gray;
      pixel->blue = (unsigned char) gray;
      pixel->green = (unsigned char) gray;
    }
  }
}

void swap_ppm(ppmImage *img) {
  print_swap();
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      ppmPixel *pixel = img->data + x + y * (img->width);
      unsigned char temp = pixel->red;
      pixel->red = pixel->blue;
      pixel->blue = pixel->green;
      pixel->green = temp;
    }
  }
}

void fill_gaus(float sigma, int n, float array[][n]) {
  int m = n / 2;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      array[i][j] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(i - m) + sq(j - m)) / (2 * sq(sigma)));
    }
  }
}

void compute_blur(ppmImage *img, int row, int col, int n, float gaus[][n], ppmPixel *update) {
  float totalRed = 0.0;
  float totalGreen = 0.0;
  float totalBlue = 0.0;
  float total = 0.0;
  int m = n / 2;
  for (int y = -1 * m; y <= m; y++) {
    if (row + y >= 0 && row + y < (int) img->height) {
      for (int x = -1 * m; x <= m; x++) {
        if (col + x >= 0 && col + x < (int) img->width) {
          ppmPixel *p = img->data + (row + y) * img->width + (col + x);
          float adjust = gaus[m + y][m + x];
          totalRed += p->red * adjust;
          totalGreen += p->green * adjust;
          totalBlue += p->blue * adjust;
          total += adjust;
        }
      }
    }
  }
  //Note no saturating math needs to be done becuase the total is
  //always going to be greater or equal to 1 so the values never get too big
  //Side Note: total should always be equal to 1 since it's a normal distrubition
  //but the gaussian matrix gets weird for super small sigma values
  ppmPixel *change = update + (row) * img->width + col;
  change->red = (char) ((totalRed / total));
  change->green = (char) ((totalGreen / total));
  change->blue = (char) ((totalBlue / total));
}

void sharpen_ppm(char *input, ppmImage *img) {
  float sigma, factor;
  if (sscanf(input, "%*s %f %f", &sigma, &factor) != 2) {
    invalid_sharpen(input);
    return;
  }
  if (sigma <= 0) {
    negative_input("sigma", sigma);
    return;
  }
  if (factor < 0) {
    negative_input("intensity", factor);
    return;
  }
  print_sharpen(sigma, factor);;
  int n = sigma * 10;
  if (n % 2 == 0) {
    n++;
  }
  float gaus[n][n];
  fill_gaus(sigma, n, gaus);
  ppmPixel *update = malloc(sizeof(ppmPixel) * img->width * img->height);
  for (int i = 0; i < (int) img->height; i++) {
    for (int j = 0; j < (int) img->width; j++) {
      compute_blur(img, i, j, n, gaus, update);
    }
  }
  for (int i = 0; i < (int) img->height; i++) {
    for (int j = 0; j < (int) img->width; j++) {
      ppmPixel *p = img->data + j + i * (img->width);
      ppmPixel *p2 = update + j + i * (img->width);
      p->red = saturate((int) (p->red + factor * (p->red - p2->red)));
      p->green = saturate((int) (p->green + factor * (p->green - p2->green)));
      p->blue = saturate((int) (p->blue + factor * (p->blue - p2->blue)));
    }
  }
  free(update);
}

int compute_intensity(int width, int x, int y, ppmPixel *data, float threshold) {
  float ix = ((data + y * width + x + 1)->red - (data + y * width + x - 1)->red) / 2;
  float iy = ((data + (y + 1) * width + x)->red - (data + (y - 1) * width + x)->red) / 2;
  if (sqrtf(ix * ix + iy * iy) > threshold) {
    return 1;
  }
  return 0;
}

void edge_ppm(char *input, ppmImage *img) {
  float sigma, threshold;
  if (sscanf(input, "%*s %f %f", &sigma, &threshold) != 2) {
    invalid_edge(input);
    return;
  }
  if (sigma <= 0) {
    negative_input("sigma", sigma);
    return;
  }
  if (threshold < 0) {
    negative_input("threshold", threshold);
  }
  print_edge(sigma, threshold);
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      ppmPixel *pixel = img->data + x + y * (img->width);
      double gray = 0.3 * pixel->red + 0.59 * pixel->green + 0.11 * pixel->blue;
      pixel->red = (unsigned char) gray;
      pixel->blue = (unsigned char) gray;
      pixel->green = (unsigned char) gray;
    }
  }
  int n = sigma * 10;
  if (n % 2 == 0) {
    n++;
  }
  float gaus[n][n];
  fill_gaus(sigma, n, gaus);
  ppmPixel *update = malloc(sizeof(ppmPixel) * img->width * img->height);
  for (int i = 0; i < (int) img->height; i++) {
    for (int j = 0; j < (int) img->width; j++) {
      compute_blur(img, i, j, n, gaus, update);
    }
  }
  for (int y = 0; y < (int) img->height; y++) {
    for (int x = 0; x < (int) img->width; x++) {
      ppmPixel *p = img->data + x + y * (img->width);
      if (x != 0 && y != 0 && y != (int) img->height - 1 && x != (int) img->width - 1 &&
        compute_intensity((int) img->width, x, y, update, threshold)) {
        p->red = 0;
        p->green = 0;
        p->blue = 0;
      } else {
        p->red = 255;
        p->green = 255;
        p->blue = 255;
      }
    }
  }
  free(update);
}

void blur_ppm(char *input, ppmImage *img) {
  float sigma;
  if (sscanf(input, "%*s %f", &sigma) != 1) {
    invalid_blur(input);
    return;
  }
  if (sigma <= 0) {
    negative_input("sigma", sigma);
    return;
  }
  print_blur(sigma);
  int n = sigma * 10;
  if (n % 2 == 0) {
    n++;
  }
  float gaus[n][n];
  fill_gaus(sigma, n, gaus);
  ppmPixel *update = malloc(sizeof(ppmPixel) * img->width * img->height);
  for (int i = 0; i < (int) img->height; i++) {
    for (int j = 0; j < (int) img->width; j++) {
      compute_blur(img, i, j, n, gaus, update);
    }
  }
  free(img->data);
  img->data = update;
}


void brightness_ppm(char *input, ppmImage *img) {
   int adjust;
   if (sscanf(input, "%*s %d",&adjust) != 1) {
     invalid_brightness(input);
     return;
   }
   print_adjust_brightness(adjust);
   for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      ppmPixel *pixel = img->data + x + y * (img->width);
      pixel->red = saturate(((int) pixel->red) + adjust);
      pixel->green = saturate(((int) pixel->green) + adjust);
      pixel->blue = saturate(((int) pixel->blue) + adjust);
    }
  }
}

void crop_ppm(char *input, ppmImage *img) {
  int x1, y1, x2, y2;
  if (sscanf(input, "%*s %d %d %d %d", &x1, &y1, &x2, &y2) != 4) {
    invalid_crop_input(input);
    return;
  }
  // check that all the points are in the range of 1 to width/height inclusive
  if (x1 < 1 || x2 < 1 || y1 < 1 || y2 < 1 || (x1 > (int) img->width) ||
    (x2 > (int) img->width) || (y1 > (int) img->height) || (y2 > (int) img-> height)) {
    crop_out_of_bounds(x1, y1, x2, y2);
    return;
  }
  //check to see that the second point is below and to the right of the first
  if (x1 > x2 || y1 > x2) {
    bad_crop_corners(x1, y1, x2, y2);
    return;
  }
  print_crop_setting(x1, y1, x2, y2);
  unsigned int width = (x2 - x1 + 1);
  unsigned int height = (y2 - y1 + 1);
  ppmPixel *array = malloc(sizeof(ppmPixel) * width * height);
  for (unsigned int j = 0; j < height; j++) {
    for (unsigned int i = 0; i < width; i++) {
      *(array + (j * width) + i) = *((img->data) + ((y1 + j- 1) * img->width) + (x1 + i - 1));
    }
  }
  free(img->data);
  img->data = array;
  img->height = height;
  img->width = width;
}

void contrast_ppm(char *input, ppmImage *img) {
  float adjust;
  if (sscanf(input, "%*s %f", &adjust) != 1) {
    invalid_contrast(input);
    return;
  }
  if (adjust < 0) {
    negative_input("contrast", adjust);
    return;
  }
  print_adjust_contrast(adjust);
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      ppmPixel *pixel = img->data + x + y * (img->width);
      pixel->red = saturate(((((float) pixel->red / 255) - 0.5) * adjust + 0.5) * 255);
      pixel->green = saturate(((((float) pixel->blue / 255) - 0.5) * adjust + 0.5) * 255);
      pixel->blue = saturate(((((float) pixel->green / 255) - 0.5) * adjust + 0.5) * 255);
    }
  }
}

void write_ppm(char *input, ppmImage *img) {
  char filename[MAX_LINE_LENGTH];
  sscanf(input, "%*s %s", filename);
  FILE *fp = fopen(filename, "wb");
  print_write_file(filename);
  if (fp != NULL) {
    fprintf(fp, "P6\n");
    fprintf(fp, "%d %d\n", img->width, img->height);
    fprintf(fp, "255\n");
    fwrite(img->data, 3 * img->width, img->height, fp);
    fclose(fp);
  } else {
    error_unable_write_file(filename);
  }
}

//return a boolean represented by an int if the image was succesffuly processed.
int read_ppm(FILE *fp, ppmImage *img) {
  int width, height, colors;
  char buffer[3], c;
  if (fgets(buffer, sizeof(buffer), fp) == NULL) {
    empty_file();
    return 0;
  }
  if (buffer[0] != 'P' || buffer[1] != '6') {
    improper_header();
    return 0;
  }
  c = fgetc(fp);
  while (c == '#' || isspace(c)) {
    if (c == '#') {
      while (fgetc(fp) != '\n');
    }
    c = fgetc(fp);
  }
  ungetc(c, fp);
  if (fscanf(fp, "%d", &width) != 1) {
    improper_width();
    return 0;
  }
  c = fgetc(fp);
  while (c == '#' || isspace(c)) {
    if (c == '#') {
      while (fgetc(fp) != '\n');
    }
    c = fgetc(fp);
  }
  ungetc(c, fp);
  if (fscanf(fp, "%d", &height) != 1) {
    improper_height();
    return 0;
  }
  c = fgetc(fp);
  while (c == '#' || isspace(c)) {
    if (c == '#') {
      while (fgetc(fp) != '\n');
    }
    c = fgetc(fp);
  }
  ungetc(c, fp);
  if (fscanf(fp, "%d", &colors) != 1) {
    improper_color();
    return 0;
  }
  if (colors != RGB_COLOR_SIZE) {
    improper_color_size();
    return 0;
  }
  c = fgetc(fp);
  while (c == '#' || isspace(c)) {
    if (c == '#') {
      while (fgetc(fp) != '\n');
    }
    c = fgetc(fp);
  }
  ungetc(c, fp);
  if (img->data != NULL) {
    free(img->data);
  }
  img->width = width;
  img->height = height;
  img->data = malloc(sizeof(ppmPixel) * width * height);
  if (fread(img->data, 3 * img->width, img->height, fp) != img->height) {
    pixel_processing();
    return 0;
  }
  return 1;
}



//returns a boolean based on whether it properly loaded an image
int read_image(char *input, ppmImage *img) {
  char filename[MAX_LINE_LENGTH];
  sscanf(input, "%*s %s", filename);
  print_read_file(filename);
  FILE *fp = fopen(filename, "rb");
  if (fp != NULL) {
    if (read_ppm(fp, img)) {
      fclose(fp);
      return 1;
    }
    fclose(fp);
  } else {
    error_unable_open_file(filename);
  }
  return 0;
}

void process_input() {
  print_menu();
  char line[MAX_LINE_LENGTH], input[MAX_LINE_LENGTH];
  fgets(line, MAX_LINE_LENGTH, stdin);
  int image_loaded = 0;
  ppmImage *img = malloc(sizeof(ppmImage));
  sscanf(line, "%s", input);
  while (strcmp(input, "q") != 0) {
    if (strcmp(input, "r") == 0) {
      if (read_image(line, img)) {
        image_loaded = 1;
      }
    } else if (strcmp(input, "w") == 0) {
      if (image_loaded) {
        write_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "s") == 0) {
      if (image_loaded) {
        swap_ppm(img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "br") == 0) {
      if (image_loaded) {
        brightness_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "c") == 0) {
      if (image_loaded) {
        crop_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "g") == 0) {
      if (image_loaded) {
        grayscale_ppm(img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "cn") == 0) {
      if (image_loaded) {
        contrast_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "bl") == 0) {
      if (image_loaded) {
        blur_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "sh") == 0) {
      if (image_loaded) {
        sharpen_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else if (strcmp(input, "e") == 0) {
      if (image_loaded) {
        edge_ppm(line, img);
      } else {
        error_no_image_loaded();
      }
    } else {
      bad_menu_option(input);
    }
    print_menu();
    fgets(line, MAX_LINE_LENGTH, stdin);
    sscanf(line, "%s", input);
  }
  if (img->data != NULL) {
    free(img->data);
  }
  free(img);
}
