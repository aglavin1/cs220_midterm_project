/**
  Alexander Glavin and Jacob Meyer
  601.220
  Midterm Project
  email: aglavin1@jhu.edu & jmeyer54@jhu.edu
  10/11/17
  jhedID: aglavin1 & jmeyer54

  File containing all functions pertaining to the interface
  This mainly mean all the functions that print to the screen
  Also included functions for reading input from the user
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "interface.h"

#define MAX_LINE_LENGTH 1024

void print_menu() {
  printf("Main menu:\n"
    "        r <filename> - read image from <filename>\n"
    "        w <filename> - write image to <filename>\n"
    "        s - swap color channels\n"
    "        br <amt> - change brightness (up or down) by the given amount\n"
    "        c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n"
    "        g - convert to grayscale\n"
    "        cn <amt> - change contrast (up or down) by the given amount\n"
    "        bl <sigma> - Gaussian blur with the given radius (sigma)\n"
    "        sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n"
    "        e <sigma> <threshold> - detect edges with intensity gradient above given threshold\n"
    "        q - quit\n"
    "Enter choice: ");
}

void error_no_image_loaded() {
  printf("Error: No image loaded yet. Please use the \"r\" command to load an image\n");
}

void bad_menu_option(char* input) {
  printf("Bad menu option: %s\n", input);
}

void print_read_file(char* filename) {
  printf("Reading from %s...\n", filename);
}

void error_unable_open_file(char* filename) {
  printf("Error: Unable to open file: %s\n", filename);
}

void error_unable_write_file(char* filename) {
  printf("Error: Unable to write to file: %s\n", filename);
}

void print_write_file(char* filename) {
  printf("Writing to %s...\n", filename);
}

void invalid_brightness(char* line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "br%[^\n]", remain);
  printf("Error: invalid brighness amount: %s", remain);
}

void print_adjust_brightness(int adjust) {
  printf("Adjusting brightness by %d...\n", adjust);
}

void invalid_contrast(char* line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "cn%[^\n]", remain);
  printf("Error: invalid contrast amount: %s\n", remain);
}

void print_adjust_contrast(float adjust) {
  printf("Adjusting contrast by %.3f...\n", adjust);
}

void invalid_crop_input(char *line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "c%[^\n]", remain);
  printf("Error: invalid crop input: %s\n", remain);
}

void crop_out_of_bounds(int x1, int y1, int x2, int y2) {
  printf("Error: crop corners (%d, %d) or (%d, %d) is out of bounds\n", x1, y1, x2, y2);
}

void bad_crop_corners(int x1, int y1, int x2, int y2) {
  printf("Error: first crop corner (%d, %d) is not above and to the left of the second crop corner (%d, %d)\n", x1, y1, x2, y2);
}

void print_crop_setting(int x1, int y1, int x2, int y2) {
  printf("Cropping region from (%d, %d) to (%d, %d)...\n", x1, y1, x2, y2);
}

void print_grayscale() {
  printf("Converting to grayscale...\n");
}

void print_swap() {
  printf("Swapping color channels...\n");
}

void empty_file() {
  printf("Error: empty file\n");
}

void improper_header() {
  printf("Error: improper header or not a ppm file\n");
}

void improper_width() {
  printf("Error: improper width value\n");
}

void improper_height() {
  printf("Error: improper height value\n");
}

void improper_color() {
  printf("Error: improper color value\n");
}

void improper_color_size() {
  printf("Error: color size is not 255\n");
}

void pixel_processing() {
  printf("Error in processing pixels in ppm file\n");
}

void invalid_blur(char *line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "bl%[^\n]", remain);
  printf("Error: invalid blur input: %s\n", remain);
}

void print_blur(float sigma) {
  printf("Applying blur filer, sigma %.3f..\n", sigma);
}

void print_sharpen(float sigma, float factor) {
  printf("Applying sharpen filter, sigma %.3f, intensity %.3f..\n", sigma, factor);
}

void invalid_sharpen(char *line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "sh%[^\n]", remain);
  printf("Error: invalid sharpen input: %s\n", remain);
}

void print_edge(float sigma, float threshold) {
  printf("Doing edge detection with sigma %.3f and threshold %.3f..\n", sigma, threshold);
}

void invalid_edge(char *line) {
  char remain[MAX_LINE_LENGTH];
  sscanf(line, "e%[^\n]", remain);
  printf("Error: invalid edge input: %s\n", remain);
}

void negative_input(char *input, float number){
  printf("Error: non-positive %s value: %.3f\n", input, number);
}
