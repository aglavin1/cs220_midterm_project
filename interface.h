/**
  Alexander Glavin and Jacob Meyer
  601.220
  Midterm Project
  email: aglavin1@jhu.edu & jmeyer54@jhu.edu
  10/11/17
  jhedID: aglavin1 & jmeyer54
*/

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdlib.h>
#include <stdio.h>

/**
  Prints the menu options
*/
void print_menu();

/**
  Warns user if trying to preform a command when no image has been loaded
*/
void error_no_image_loaded();

/**
  Warns user if a bad menu option is used
*/
void bad_menu_option(char* input);

/**
  Tells user what the read function is trying to read from
*/
void print_read_file(char* filename);

/**
  Alerts user if a file is unabled to be opened
*/
void error_unable_open_file(char* filename);

/**
  Alerts user if the program was unable to write to a file
*/
void error_unable_write_file(char* filename);

/**
  Tells the user what file is being written to
*/
void print_write_file(char* filename);

/**
  Alerts the user if an invalid brightness specification is used
*/
void invalid_brightness(char* brightness);

/**
  Tells the user how the brightness is beign adjusted by
*/
void print_adjust_brightness(int brightness);

/**
  Alerts the user if an invalid contrast specification is used
*/
void invalid_contrast(char* contrast);

/**
  Tells the user how the brightness is beign adjusted by
*/
void print_adjust_contrast(float contrast);

/**
  Error message for invalid crop input
*/
void invalid_crop_input(char *crop);

/**
  Error message for if the crop corners are out of bounds
*/
void crop_out_of_bounds(int x1, int y1, int x2, int y2);

/**
  Error message if first crop corner is not above and to the left of the first
*/
void bad_crop_corners(int x1, int y1, int x2, int y2);

/**
  Tells the user what the specified crop corners are
*/
void print_crop_setting(int x1, int y1, int x2, int y2);

/**
  print that a swap is being done
*/
void print_swap();

/**
  Print that a grayscale conversion if being done
*/
void print_grayscale();

/**
  Print if the file is empty
 */
void empty_file();

/**
  Print if wrong header or not ppm file
 */
void improper_header();

/**
  Print if improper width
 */
void improper_width();

/**
  Print if improper height
 */
void improper_height();

/**
  Print if improper color value
 */
void improper_color();

/**
  Print if improper color size
 */
void improper_color_size();

/**
  Print if error in processing pixles
 */
void pixel_processing();

/**
  Alerts user if there's an invalid blur input
*/
void invalid_blur(char *input);

/**
  Print the sigma value being used for the blur function
*/
void print_blur(float sigma);

/**
  Alerts user if there's an invalid blur input
*/
void invalid_sharpen(char *input);

/**
  Print the sigma value being used for the blur function
*/
void print_sharpen(float sigma, float factor);

/**
  Print the sigma and threshold value beign used for edge detection
*/
void print_edge(float sigma, float threshold);

/**
  Alert user if an invalid input is used for edge detection
*/
void invalid_edge(char *input);

/**
  Improper negative value for an input
 */
void negative_input(char *input, float sigma);

#endif
